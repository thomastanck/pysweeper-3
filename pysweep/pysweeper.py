from .pysweep import mw

from .gamemode import GameMode
from .gamemodemanager import GameModeManager

from .gamedisplay import GameDisplay

class PySweeper(GameMode):
    def game_mode_name(self):       return "PySweeper"
    def game_mode_priority(self):   return -1
    def game_mode_enable(self):
        self.gamedisplay = GameDisplay(mw.tk)

    def game_mode_disable(self):
        self.gamedisplay.displaycanvas.pack_forget()
        del self.gamedisplay
GameModeManager.register_game_mode(PySweeper)
