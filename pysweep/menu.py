import tkinter

class Menu:
    """
    Instantiate this class if you want your window to have a menu,
    but only one instance per window!

    `menu = Menu(window); menu.add_menu('stuff', menutab)`
    """
    def __init__(self, tkobj=None):
        if tkobj:
            self.init_menu(tkobj)

    def init_menu(self, tkobj):
        self._tkobj = tkobj

        self._menubar = tkinter.Menu(self._tkobj)
        self._tkobj.config(menu=self._menubar)

    def add_menu(self, label, menu):
        self._menubar.add_cascade(label=label, menu=menu)

    def remove_menu(self, label, menu):
        self._menubar.delete(label)

    @property
    def menubar(self):
        return self._menubar
