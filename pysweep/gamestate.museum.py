"""
Some old code that creates a live connection between GameDisplay and an array of sorts.

Really dumb idea in the end
because the real issue was not property like behaviour
but instead was more about ease of manipulation.
Setting counters and such are really easy,
but the main goal was supposed to be to be able to update the display in one go.
In addition, manipulating boards was also important,
but having an array like interface does not actually help with manipulating boards.
"""

"""
A test game mode that tested GameState
"""
class TestGameMode(GameMode):
    def __init__(self):
        self.formatstring = 'method binding \t{}\t{}\t{}'
    def game_mode_name(self):       return "Test Game Mode"
    def game_mode_priority(self):   return 0
    def game_mode_enable(self):
        print('eyy lmao')
        self.gamedisplay = GameDisplay(mw.tk)

        self._gs = GameState(display=self.gamedisplay)
        print("self._gs.board",                '\t',     self._gs.board)
        print("self._gs.face",                 '\t',     self._gs.face)
        print("self._gs.board[0]",             '\t',     self._gs.board[0])
        print("self._gs.board[0][0]",          '\t',     self._gs.board[0][0])
        print("self._gs.board[:]",             '\t',     self._gs.board[:])
        print("self._gs.board[:][0]",          '\t',     self._gs.board[:][0])
        print("self._gs.board[0][:]",          '\t',     self._gs.board[0][:])
        print("self._gs.board[0][:][0]",       '\t',     self._gs.board[0][:][0])
        print("type(self._gs.board)",          '\t',     type(self._gs.board))
        print("type(self._gs.face)",           '\t',     type(self._gs.face))
        print("type(self._gs.board[0])",       '\t',     type(self._gs.board[0]))
        print("type(self._gs.board[0][0])",    '\t',     type(self._gs.board[0][0]))
        print("type(self._gs.board[:])",       '\t',     type(self._gs.board[:]))
        print("type(self._gs.board[:][0])",    '\t',     type(self._gs.board[:][0]))
        print("type(self._gs.board[0][:])",    '\t',     type(self._gs.board[0][:]))
        print("type(self._gs.board[0][:][0])", '\t',     type(self._gs.board[0][:][0]))

        self._gs.board[2] = [TileState.Flag] * 30
        self._gs.board[3][4] = TileState.Flag
        self._gs.board[4][2:5] = [TileState.Flag] * 3
        self._gs.board[10:13] = [[TileState.Flag] * 30] + [[TileState.Mine] * 30] + [[TileState.Number[3]] * 30]

        # self._gs.board = [[TileState.Number[5]] * 30] * 16

        self.methodbindingtest.register()
        # normalbindingtest.register()

    def game_mode_disable(self):
        print('dying...')
        self.gamedisplay.displaycanvas.pack_forget()
        del self.gamedisplay

        self.methodbindingtest.unregister()
        # Comment the above line to test having multiple bindings to the same method!
        # They both use methodbindingtest, the exact same Binding object,
        # but they're attached to two different GameMode's, and have different self.formatstring,
        # and the self-bindings for them are different! \o/
        # To test, switch between PySweeper and PySweeper Variant game modes.
        # As the binding isn't unregistered,
        # there will then be two different bindings registered by the same Binding object,
        # pointing to different objects (game modes)

        # normalbindingtest.unregister()

    @binding('<ButtonPress-1>')
    def methodbindingtest(self, e):
        self._gs.board[0][1] = TileState.Flag if self._gs.board[0][1] == TileState.Unopened else TileState.Unopened
        self._gs.lcounter, self._gs.rcounter = e.x_root, e.y_root
        print(self.formatstring.format(e.x_root, e.y_root, e.widget))
GameModeManager.register_game_mode(TestGameMode)



"""
GameState stuff
"""

class _TileHandler:
    def __init__(self, row, col, display=None):
        self._row = row
        self._col = col
        self._state = TileState.Unopened
        self._display = display

    def __get__(self, instance, owner):
        if self._display:
            self._state = self._display.get_tile((self._row, self._col))
        return self._state
    def __set__(self, instance, value):
        self._state = value
        if self._display:
            self._display.set_tile((self._row, self._col), self._state)

class BoardProxy:
    def __init__(self, boardsize, display=None):
        self._numcols, self._numrows = boardsize
        self._display = display
        self._rows = [RowProxy(row, self._numcols, display) for row in range(self._numrows)]

    def __getitem__(self, key):
        return self._rows[key]

    def __setitem__(self, key, value):
        if isinstance(key, slice):
            for item, newitem in zip(self._rows[key], value):
                item[:] = newitem
        else:
            self._rows[key][:] = value

    def __len__(self):
        return self._numrows

class RowProxy:
    def __init__(self, row, numcols, display=None):
        self._row = row
        self._numcols = numcols
        self._display = display
        self._tiles = [_TileHandler(self._row, col, display) for col in range(self._numcols)]

    def __getitem__(self, key):
        if isinstance(key, slice):
            output = []
            for tilehandler in self._tiles[key][:]:
                output.append(tilehandler.__get__(None, None))
            return output
        else:
            return self._tiles[key].__get__(None, None)
    def __setitem__(self, key, value):
        if isinstance(key, slice):
            for item, newitem in zip(self._tiles[key], value):
                item.__set__(None, newitem)
        else:
            self._tiles[key].__set__(None, value)

    def __len__(self):
        return self._numcols

class GameState:
    """
    Helper class between logic layer and UI layer

    Makes it easier to manipulate the UI
    by allowing you to simply set properties
    rather than having to use getters and setters.

    Main benefit of this is really the ability to set the entire board at once...
    Actually why did I write this class at all xP

    Check out testgamemodes.py to see the shenanigans you can do with this.
    """
    def __init__(self, boardsize=(30,16), display=None):
        self._display = display
        self._boardsize = boardsize
        self._board = BoardProxy(boardsize, display)

        self._lcounter = self._rcounter = 0
        self._face = FaceState.Happy

    @property
    def board(self):
        return self._board
    @board.setter
    def board(self, newboard):
        assert len(newboard) == len(self._board), 'GameState.board_setter: # of rows in board do not match'
        for i in range(len(newboard)):
            assert len(newboard[i]) == len(self._board[i]), 'GameState.board_setter: # of cols in board do not match'

        self._board[:] = newboard

    @property
    def lcounter(self):
        if self._display:
            self._lcounter = int(self._display.get_lcounter())
        return self._lcounter
    @property
    def rcounter(self):
        if self._display:
            self._rcounter = int(self._display.get_rcounter())
        return self._rcounter
    @property
    def face(self):
        if self._display:
            self._face = self._display.get_face()
        return self._face

    @lcounter.setter
    def lcounter(self, value):
        self._lcounter = value
        if self._display:
            self._display.set_lcounter(self._lcounter)
    @rcounter.setter
    def rcounter(self, value):
        self._rcounter = value
        if self._display:
            self._display.set_rcounter(self._rcounter)
    @face.setter
    def face(self, value):
        self._face = value
        if self._display:
            self._display.set_face(self._face)

