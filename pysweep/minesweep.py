import itertools

from .array import Array

from .gamedisplay import TileState

def _in_board(tile, boardsize):
    return 0 <= tile[0] < boardsize[1] and 0 <= tile[1] < boardsize[0]
def _is_corner(tile, boardsize):
    return (0 == tile[0] or tile[0] == boardsize[1]-1) and (0 == tile[1] or tile[1] == boardsize[0]-1)
def _is_edge(tile, boardsize):
    return (0 == tile[0] or tile[0] == boardsize[1]-1) or (0 == tile[1] or tile[1] == boardsize[0]-1) and not _is_corner(tile, boardsize)

class OUBError(KeyError):
    def __init__(self):
        super().__init__(self, 'Tile index outside board.')
class RemoveMineError(RuntimeError):
    def __init__(self):
        super().__init__(self, 'Cannot remove mine as the tile is not a mine.')
class InvalidTileError(RuntimeError):
    def __init__(self):
        super().__init__(self, 'Tried to get invalid tile. All tiles around this tile must be generated before it can be getted.')

class Field:
    """ The absolute core logic for minesweeper. Name comes from "mine field". """

    def __init__(self, boardsize):
        self._boardsize = boardsize
        self._mines = set()
        self._nummines = 0
        self._numbers = Array(*boardsize, 0)
        # self._numbers = [[0 for i in range(boardsize[0])] for j in range(boardsize[1])]
    def add_mine(self, tile):
        if not _in_board(tile, self._boardsize): raise OUBError()
        if tile in self._mines:
            return
        self._mines.add(tile)
        self._nummines += 1
        for i in range(-1, 2):
            for j in range(-1, 2):
                if _in_board((tile[0]+i, tile[1]+j), self._boardsize):
                    self._numbers[tile[0]+i][tile[1]+j] += 1
    def remove_mine(self, tile):
        # if not _in_board(tile, self._boardsize): raise OUBError()
        # if tile not in self._mines: raise RemoveMineError()
        self._mines.remove(tile)
        self._nummines -= 1
        for i in range(-1, 2):
            for j in range(-1, 2):
                if _in_board((tile[0]+i, tile[1]+j), self._boardsize):
                    self._numbers[tile[0]+i][tile[1]+j] -= 1

    def get(self, tile):
        # if not _in_board(tile, self._boardsize): raise OUBError()
        if tile in self._mines:
            return TileState.Mine
        else:
            return TileState.Number[self.get_number(tile)]

    def get_number(self, tile):
        # if not _in_board(tile, self._boardsize): raise OUBError()
        return self._numbers[tile[0]][tile[1]]

    @property
    def nummines(self):
        return self._nummines


class Sweep:
    """ Core logic as well as mine generation/validity handling. """
    def __init__(self, boardsize):
        self._boardsize = boardsize

        self._field = Field(boardsize)
        self._generated = set()
        self._valid = [
            [4 if _is_corner((j,i),boardsize) else
             (6 if _is_edge((j,i), boardsize) else 9)
                for i in range(boardsize[0])]
            for j in range(boardsize[1])] # If all tiles around each tile have been generated, this number will be 0

    def generate(self, tile, ismine):
        if not _in_board(tile, self._boardsize): raise OUBError()
        if tile in self._generated:
            return
        if ismine:
            self._field.add_mine(tile)
        self._generated.add(tile)
        for i in range(-1, 2):
            for j in range(-1, 2):
                if _in_board((tile[0]+i, tile[1]+j), self._boardsize):
                    self._valid[tile[0]+i][tile[1]+j] -= 1

    def get(self, tile):
        # if not _in_board(tile, self._boardsize): raise OUBError()
        # if not self.is_valid(tile): raise InvalidTileError()
        return self._field.get(tile)

    def get_number(self, tile):
        # if not _in_board(tile, self._boardsize): raise OUBError()
        # if not self.is_valid(tile): raise InvalidTileError()
        return self._field.get_number(tile)

    def is_valid(self, tile):
        return self._valid[tile[0]][tile[1]] == 0
    def is_generated(self, tile):
        return tile in self._generated

    def is_mine(self, tile):
        return self._field.get(tile) == TileState.Mine

    @property
    def nummines(self):
        return self._field.nummines
    @property
    def numgenerated(self):
        return len(self._generated)


class Minesweep:
    """ All basic minesweeper logic for a basic minesweeper game mode. """
    def __init__(self, boardsize=(30, 16), targetmines=99, autogenerate=True, rng=None):
        self._boardsize = boardsize
        self._targetmines = targetmines
        self._autogenerate = autogenerate
        if autogenerate:
            if rng:
                self._rng = rng
            else:
                from .pythonrand import PythonRand
                self._rng = PythonRand()
        else:
            self._rng = None

        self._sweep = Sweep(boardsize)
        self._opened = Array(*boardsize, False)
        self._flagged = Array(*boardsize, False)
        # self._opened = [[False for i in range(boardsize[0])] for j in range(boardsize[1])]
        # self._flagged = [[False for i in range(boardsize[0])] for j in range(boardsize[1])]
        self._notgenerated = set(itertools.product(range(boardsize[1]), range(boardsize[0])))
        self._numopened = 0
        self._numflagged = 0

    def open(self, tile):
        """
        Returns true if a tile was opened,
        false if it was already open or is a flag.
        """
        # print('open', tile)
        # if not _in_board(tile, self._boardsize): raise OUBError()
        # if not self._sweep.is_valid(tile) and not self._autogenerate:
            # raise InvalidTileError()

        if self._opened[tile[0]][tile[1]] or self._flagged[tile[0]][tile[1]]:
            return False

        self._opened[tile[0]][tile[1]] = True
        self._numopened += 1
        if not self._sweep.is_valid(tile):
            if self._autogenerate:
                for i in range(-1, 2):
                    for j in range(-1, 2):
                        gentile = (tile[0]+i, tile[1]+j)
                        if gentile in self._notgenerated:
                        # if not _in_board(gentile, self._boardsize):
                        #     continue
                            self.autogenerate(gentile)
            else:
                raise InvalidTileError()

        return True

    def toggle_flag(self, tile):
        # if not _in_board(tile, self._boardsize): raise OUBError()

        if self._opened[tile[0]][tile[1]]:
            return self.get(tile)

        if self._flagged[tile[0]][tile[1]]:
            self._flagged[tile[0]][tile[1]] = False
            self._numflagged -= 1
        else:
            self._flagged[tile[0]][tile[1]] = True
            self._numflagged += 1

        return self.get(tile)

    def generate(self, tile, ismine):
        self._sweep.generate(tile, ismine)
        self._notgenerated.remove(tile)

    def autogenerate(self, tile):
        if not _in_board(tile, self._boardsize): raise OUBError()
        if self._sweep.is_generated(tile):
            return
        ismine = self._rng.random(
            self._targetmines - self._sweep.nummines,
            self._boardsize[0]*self._boardsize[1] - self._sweep.numgenerated
        )
        self.generate(tile, ismine)

    def get(self, tile):
        # if not _in_board(tile, self._boardsize): raise OUBError()
        if self._flagged[tile[0]][tile[1]]:
            return TileState.Flag
        elif not self._opened[tile[0]][tile[1]]:
            return TileState.Unopened
        else:
            return self._sweep.get(tile)

    def is_mine(self, tile):
        return self._sweep.is_mine(tile)

    @property
    def boardsize(self):
        return self._boardsize

    @property
    def minesleft(self):
        return self._targetmines - self._numflagged
    @property
    def numopened(self):
        return self._numopened
    @property
    def numflagged(self):
        return self._numflagged
    @property
    def targetmines(self):
        return self._targetmines


