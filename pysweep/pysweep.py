class PySweep:
    """
    Global scope-class singleton for all variables that need to be global.
    """

    def __init__(self, tk=None):
        self.tk = None
        """
        The tkinter.Tk instance.
        """

        self.menu = None
        """
        The .menu.Menu of the window.
        """

        self.listener = None
        """
        The .listener.Listener of the window.
        """

        self.playerinput = None
        """
        The .playerinput.PlayerInput of the window.
        """

        if tk:
            self.init(tk)

    def init(self, tk):
        if self.tk:
            raise RuntimeError('PySweep already initialised with a window')
        self.tk = tk
        from .menu import Menu
        from .listener import Listener
        from .playerinput import PlayerInput
        self.menu = Menu(tk)
        self.listener = Listener(tk)
        self.playerinput = PlayerInput(self.listener)

mw = PySweep()
"""
The PySweep object for the main window.

Initialised in __main__.py
"""
