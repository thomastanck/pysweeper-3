from .playerinput import ButtonState, Button, Action, ButtonAction

class ClickMode:
    """
    poor man's enum for keeping track of chords.

    This is because actual enums either need an external pip package or
    Python 3.4+
    """
    class Released:
        """
        State when all mouse buttons are up.

        In this state, RD toggles flag.
        """
        pass
    class BeforeChord:
        """
        Activated when a LD/RD arrives while in Released.

        In this state, LU opens a single tile.

        In this state, if LMB is down, the tile under the cursor gets
        a depressed animation.
        """
        pass
    class Chord:
        """
        Activated when a LD/MD/RD arrives while in BeforeChord, or when a MD
        arrives while in Released.

        Re-activated when a LD/MD/RD arrives while in AfterChord.

        In this state, LU/MU/RU performs a chord (opens all neighbours if the
        clicked tile is fulfilled).

        In this state, the tile under the cursor and its neighbours all get
        a depressed animation.
        """
        pass
    class AfterChord:
        """
        Activated when LU/MU/RU arrives while in Chord AND at least
        one mouse button is depressed (because Released is activated if there
        were no more buttons depressed).

        In this state, no actions are performed, and there are no depressed
        animations.
        """
        pass

class BoardAction:
    """
    poor man's enum for board actions performed after a click event.

    This is because actual enums either need an external pip package or
    Python 3.4+
    """
    class Open:
        """
        Send out a TileOpen event if cursor is over a tile. (and not the panel, etc.)
        """
        pass
    class Flag:
        """
        Send out a Flag event if cursor is over a tile.
        """
        pass
    class Chord:
        """
        Send out a TileChord event if cursor is over a tile.
        """
        pass

class BoundaryAction:
    class Enter:
        """
        When the cursor enters a tile
        """
        pass
    class Leave:
        """
        When the cursor leaves a tile
        """
        pass

class ButtonPressAction:
    """
    Actions that relate to a clickable button.
    """
    class Press:
        """
        When the button should appear depressed.
        """
        pass
    class Lift:
        """
        When the button should appear undepressed.
        """
        pass
    class Click:
        """
        When a LU happens on a button (it is clicked, activated.)
        """
        pass

class BoardPressAction:
    """
    Actions that relate to depressing tiles on a board.

    Note: When a press transitions to a chordpress, there is no Lift.
    Lift happens when going from Chording into AfterChord
    or when going from Released to BeforeChord.

    These events are not tile-centric.
    If a tile is pressed then the cursor is dragged out of it,
    there will be a Press/ChordPress but not a Lift.
    """
    class Press: pass
    class ChordPress: pass
    class Lift: pass

# Some convenience functions
def is_button_down(buttonstate):
    return buttonstate > ButtonState.Released
def num_buttons_down(playerinputevent):
    i = 0
    for n in range(3):
        if is_button_down(playerinputevent.state[n]):
            i += 1
    return i

class BoardEvent:
    def __init__(self, action, tile, state, mode):
        """
        action can either be
        a .playerinput.ButtonAction,
        a .boardinput.GameAction,
        or a .boardinput.TileAction.

        tile is a (row, col) tuple.

        state is the same as in a .playerinput.PlayerInputEvent,
        a list of 3 ButtonState's corresponding to the LMB, MMB, and RMB states.

        mode is the ClickMode,
        which can be used to figure out depressed animations.
        """
        self.action = action
        self.tile = tile
        self.state = state

    def __str__(self):
        return '{'+"action:'{}',tile:{},state:{}".format(self.action, self.tile, self.state)+'}'

    def __repr__(self):
        return str(self)

class GameEvent:
    def __init__(self, action, position, state):
        """
        action can either be any kind of Action, such as
        a .playerinput.ButtonAction,
        a .boardinput.BoardAction,
        or a .boardinput.BoundaryAction.

        position is a (x, y) tuple.

        state is the same as in a .playerinput.PlayerInputEvent,
        a list of 3 ButtonState's corresponding to the LMB, MMB, and RMB states.
        """
        self.action = action
        self.position = position
        self.state = state

    def __str__(self):
        return '{'+"action:'{}',position:{},state:{}".format(self.action, self.position, self.state)+'}'

    def __repr__(self):
        return str(self)

class BoardInput:
    """
    A class that takes PlayerInput events
    and turns them into events relating to tiles.

    For example, events for if a tile was chorded on,
    or if a tile was opened,
    or if a tile was flagged,
    or if a tile was dragged in or dragged out of, etc.
    """
    def __init__(self, playerinput, tkobj, boardtiles=None):
        """
        Needs a PlayerInput object to listen for events
        as well as a tkinter object (a Canvas usually) to be able to normalise the coordinates
        and optionally a .gamedisplay.BoardTiles object
        to figure out the offset
        of the top left of the board
        from the top left of the tkinter object

        If no Board object is provided,
        it will assume the tkobj is a .gamedisplay.DisplayCanvas
        and try to access tkobj.display.board.tiles for the object.
        """
        self._playerinput = playerinput
        self._tkobj = tkobj
        if boardtiles:
            self._boardtiles = boardtiles
        else:
            self._boardtiles = tkobj.display.board.tiles

        self._clickmode = ClickMode.Released
        self._overtile = None # Which tile the cursor is currently over.
        self._callbacks = []

    def register(self, callback):
        if len(self._callbacks) == 0:
            self._register_bindings()
        self._callbacks.append(callback)

    def unregister(self, callback):
        self._callbacks.remove(callback)
        if len(self._callbacks) == 0:
            self._unregister_bindings()

    def _register_bindings(self):
        self._playerinput.register(self._handler)

    def _unregister_bindings(self):
        self._playerinput.unregister(self._handler)

    def _handler(self, playerinputevent):
        boardaction = self._process_click(playerinputevent)

        x_root, y_root = playerinputevent.root_position
        x = x_root - self._tkobj.winfo_rootx() - self._boardtiles.position[0]
        y = y_root - self._tkobj.winfo_rooty() - self._boardtiles.position[1]

        x_tilesize, y_tilesize = self._boardtiles.images.size

        row = y // y_tilesize
        col = x // x_tilesize
        tile = (row, col)

        events = []

        if (col < 0 or col >= self._boardtiles.boardsize[0] or
            row < 0 or row >= self._boardtiles.boardsize[1]):
            # Mouse not over the board
            if self._overtile != None:
                # We were in the board before, handle leave
                events.append(BoardEvent(BoundaryAction.Leave, self._overtile, playerinputevent.state, self._clickmode))
                if (self._clickmode == ClickMode.BeforeChord and is_button_down(playerinputevent.state[0]) or
                    self._clickmode == ClickMode.Chord):
                    # It was depressed before, so we should lift.
                    events.append(BoardEvent(BoardPressAction.Lift, self._overtile, playerinputevent.state, self._clickmode))
                self._overtile = None
        else:
            # Mouse over the board, handle board actions as well as enter/leave actions

            # Basic mouse events
            events.append(BoardEvent(playerinputevent.buttonaction, tile, playerinputevent.state, self._clickmode))

            # Board actions
            if boardaction:
                events.append(BoardEvent(boardaction, tile, playerinputevent.state, self._clickmode))

            # Boundary action
            if self._overtile != tile:
                if self._overtile != None:
                    events.append(BoardEvent(BoundaryAction.Leave, self._overtile, playerinputevent.state, self._clickmode))
                self._overtile = tile
                events.append(BoardEvent(BoundaryAction.Enter, self._overtile, playerinputevent.state, self._clickmode))

                # Change tiles, refresh Press/ChordPress
                if self._clickmode == ClickMode.BeforeChord and is_button_down(playerinputevent.state[0]):
                    events.append(BoardEvent(BoardPressAction.Press, self._overtile, playerinputevent.state, self._clickmode))
                if self._clickmode == ClickMode.Chord:
                    events.append(BoardEvent(BoardPressAction.ChordPress, self._overtile, playerinputevent.state, self._clickmode))

            # Check for Press/ChordPress when button pressed down.
            if playerinputevent.buttonaction == ButtonAction.LD:
                if self._clickmode == ClickMode.BeforeChord and is_button_down(playerinputevent.state[0]):
                    events.append(BoardEvent(BoardPressAction.Press, self._overtile, playerinputevent.state, self._clickmode))
            if playerinputevent.buttonaction == ButtonAction.D:
                if self._clickmode == ClickMode.Chord:
                    events.append(BoardEvent(BoardPressAction.ChordPress, self._overtile, playerinputevent.state, self._clickmode))
            # Check for Lift when button released
            if playerinputevent.buttonaction == ButtonAction.U:
                if (self._clickmode == ClickMode.Released or
                    self._clickmode == ClickMode.AfterChord):
                    events.append(BoardEvent(BoardPressAction.Lift, self._overtile, playerinputevent.state, self._clickmode))

        # Callback away!
        for callback in self._callbacks:
            for event in events:
                callback(event)

    def _process_click(self, playerinputevent):
        """
        Sets self._clickmode and returns the BoardAction.
        """
        if self._clickmode == ClickMode.Released:
            # Released
            if playerinputevent.buttonaction == ButtonAction.LD:
                self._clickmode = ClickMode.BeforeChord
                return None
            elif playerinputevent.buttonaction == ButtonAction.MD:
                self._clickmode = ClickMode.Chord
                return None
            elif playerinputevent.buttonaction == ButtonAction.RD:
                self._clickmode = ClickMode.BeforeChord
                return BoardAction.Flag
            else:
                return None
        elif self._clickmode == ClickMode.BeforeChord:
            # BeforeChord
            if playerinputevent.buttonaction == ButtonAction.LU:
                self._clickmode = ClickMode.Released
                return BoardAction.Open
            elif num_buttons_down(playerinputevent) == 0 and playerinputevent.button != Button.Any:
                self._clickmode = ClickMode.Released
                return None
            elif num_buttons_down(playerinputevent) >= 2:
                self._clickmode = ClickMode.Chord
                return None
            else:
                return None
        elif self._clickmode == ClickMode.Chord:
            # Chord
            if playerinputevent.action == Action.U:
                if num_buttons_down(playerinputevent) == 0:
                    self._clickmode = ClickMode.Released
                else:
                    self._clickmode = ClickMode.AfterChord
                return BoardAction.Chord
            else:
                return None
        elif self._clickmode == ClickMode.AfterChord:
            # AfterChord
            if playerinputevent.action == Action.D:
                self._clickmode = ClickMode.Chord
                return None
            elif num_buttons_down(playerinputevent) == 0:
                self._clickmode = ClickMode.Released
                return None
            else:
                return None
        else:
            raise RuntimeError('Invalid internal state: clickmode is not a ClickMode. Got: {}'.format(self._clickmode))

class PartInput:
    """
    A class that takes PlayerInput events
    and turns them into events relating to a widget.

    Unlike BoardInput, this just has simple things such as
    Enter/Leave events and click events.
    """
    def __init__(self, playerinput, tkobj, part):
        self._playerinput = playerinput
        self._tkobj = tkobj
        self._part = part

        self._previousposition = None
        self._callbacks = []

    def register(self, callback):
        if len(self._callbacks) == 0:
            self._register_bindings()
        self._callbacks.append(callback)

    def unregister(self, callback):
        self._callbacks.remove(callback)
        if len(self._callbacks) == 0:
            self._unregister_bindings()

    def _register_bindings(self):
        self._playerinput.register(self._handler)

    def _unregister_bindings(self):
        self._playerinput.unregister(self._handler)

    def _handler(self, playerinputevent):
        x_root, y_root = playerinputevent.root_position
        x = x_root - self._tkobj.winfo_rootx() - self._part.position[0]
        y = y_root - self._tkobj.winfo_rooty() - self._part.position[1]

        position = (x, y)

        x_size, y_size = self._part.size

        events = []

        if (x < 0 or x > x_size or
            y < 0 or y > y_size):
            # Mouse not over the part
            if self._previousposition != None:
                # Leaving the part
                events.append(GameEvent(BoundaryAction.Leave, self._previousposition, playerinputevent.state))
                if is_button_down(playerinputevent.state[0]):
                    # LMB was down, so it was pressed before and we need to lift.
                    events.append(GameEvent(ButtonPressAction.Lift, position, playerinputevent.state))
                self._previousposition = None
        else:
            # Mouse inside the part

            # Basic mouse actions
            events.append(GameEvent(playerinputevent.buttonaction, position, playerinputevent.state))

            # Boundary action
            if self._previousposition == None:
                # Entering the part
                events.append(GameEvent(BoundaryAction.Enter, position, playerinputevent.state))

                # Check for button press
                if is_button_down(playerinputevent.state[0]):
                    # LMB is down, we need to press
                    events.append(GameEvent(ButtonPressAction.Press, position, playerinputevent.state))
            self._previousposition = position

            if playerinputevent.buttonaction == ButtonAction.D:
                # Check for button press
                if is_button_down(playerinputevent.state[0]):
                    # LMB is down, we need to press
                    events.append(GameEvent(ButtonPressAction.Press, position, playerinputevent.state))
            if playerinputevent.buttonaction == ButtonAction.LU:
                events.append(GameEvent(ButtonPressAction.Lift, position, playerinputevent.state))
                events.append(GameEvent(ButtonPressAction.Click, position, playerinputevent.state))

        # Callback away!
        for callback in self._callbacks:
            for event in events:
                callback(event)
