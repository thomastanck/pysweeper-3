import functools

class Binding:
    def __init__(self, trigger, func, listener=None):
        self._listener = listener
        self._trigger = trigger
        self._func = func
        self._currentouterself = None
        self._outerself = None
        self._boundfuncs = {}
    def __get__(self, obj, objtype):
        """
        Used in conjunction with .register().

        Saves the owner of the binding into `_outerself`
        so register knows whether to call `_func` as a bound method or not.

        If `binding.register()` was used, obj will be None,
        and as `register` will be called immediately after,
        it'll see that `_outerself` is None and not bind it.

        If `obj.binding.register()` was used, obj will be obj,
        and register will see that `_outerself` is not None and bind it.
        """
        self._outerself = obj
        return self
    def init(self, listener):
        if self._listener:
            raise RuntimeError('Binding already initialised with a listener.')
        self._listener = listener
    def register(self):
        if not self._listener:
            raise RuntimeError('Binding used before being initialised with a listener.')
        if self._outerself:
            boundfunc = functools.partial(self._handler, outerself=self._outerself)
        else:
            boundfunc = self._handler
        self._boundfuncs[self._outerself] = boundfunc
        self._listener.register(self._trigger, boundfunc)
    def unregister(self):
        if not self._listener:
            raise RuntimeError('Binding used before being initialised with a listener.')
        self._listener.unregister(self._trigger, self._boundfuncs[self._outerself])
    def _handler(self, event, outerself=None):
        if outerself:
            return self._func(outerself, event)
        else:
            return self._func(event)

def unassigned_binding(trigger):
    def _decorator(func):
        """
        Creates a Binding object without an assigned listener.

        Call `.init(listener)` before use.
        """
        return Binding(trigger, func)
    return _decorator

def binding(listener, trigger):
    def _decorator(func):
        """ Decorator to wrap a function or a class method to register it with Listener

        Example:
        ```
        @listener.binding('<KeyPress>')
        def on_key_press(e):
            print('Key {} was pressed!'.format(e.keycode))

        on_key_press.register()
        # Listener now active
        on_key_press.unregister()
        # Listener now inactive
        # Can still call like function
        on_key_press(fakedevent)
        ```

        If the function is a class method
        and you'd like it to receive self,
        pass self to .register
        and it'll be provided to the function on every call.

        Example:
        ```
        @listener.binding('<KeyPress>')
        def on_key_press(self, e):
            print(self.fmtstring.format(e.keycode))

        def other_methods(self):
            self.on_key_press.register(self)
            # Is active, self will be provided in addition to the event.
        def another_method(self):
            self.on_key_press.unregister()
            self.on_key_press(fakedevent)
            # Can still be called directly like a function.
        ```
        """
        return Binding(trigger, func, listener)
    return _decorator

class Listener:
    """
    Instantiate this class if you want your window to have a menu,
    but only one instance per window!

    `listener = Listener(window); listener.register('<Motion>', func)`
    """
    def __init__(self, tkobj=None):
        self._tkobj = tkobj
        self._triggers = {}
        self._enabled = True

    def enable(self):
        if not self._enabled:
            for trigger in self._triggers:
                self._bind(trigger, func)
            self._enabled = True
    def disable(self):
        if self._enabled:
            for trigger in self._triggers:
                self._unbind(trigger)
            self._enabled = False

    def register(self, trigger, func):
        if trigger in self._triggers:
            self._triggers[trigger].append(func)
        else:
            self._triggers[trigger] = [func]
            if self._enabled:
                self._bind(trigger)
        return (trigger, func)
    def unregister(self, trigger, func):
        self._triggers[trigger].remove(func)
        if len(self._triggers[trigger]) == 0:
            del self._triggers[trigger]
            self._unbind(trigger)

    def _bind(self, trigger):
        def _handler(event):
            for func in self._triggers[trigger]:
                func(event)
        self._tkobj.bind(trigger, _handler)
    def _unbind(self, trigger):
        self._tkobj.unbind(trigger)

    def binding(self, trigger):
        return binding(self, trigger)
