import time as stdtime

def time():
    """ Time in milliseconds """
    return int(stdtime.time() * 1000)

def seconds():
    """ Time in seconds """
    return int(stdtime.time())
