from ..pysweep import mw

from ..gamemode import GameMode
from ..gamemodemanager import GameModeManager

from ..gamedisplay import GameDisplay

@mw.listener.binding('<Motion>')
def normalbindingtest(e):
    print('binding        \t{}\t{}\t{}'.format(e.x_root, e.y_root, e.widget))

class TestGameMode(GameMode):
    def __init__(self):
        self.formatstring = 'method binding \t{}\t{}\t{}'
    def game_mode_name(self):       return "Test Game Mode"
    def game_mode_priority(self):   return 0
    def game_mode_enable(self):
        print('eyy lmao')
        self.gamedisplay = GameDisplay(mw.tk)

        self.methodbindingtest.register()
        # normalbindingtest.register()

    def game_mode_disable(self):
        print('dying...')
        self.gamedisplay.displaycanvas.pack_forget()
        del self.gamedisplay

        self.methodbindingtest.unregister()
        # Comment the above line to test having multiple bindings to the same method!
        # They both use methodbindingtest, the exact same Binding object,
        # but they're attached to two different GameMode's, and have different self.formatstring,
        # and the self-bindings for them are different! \o/
        # To test, switch between PySweeper and PySweeper Variant game modes.
        # As the binding isn't unregistered,
        # there will then be two different bindings registered by the same Binding object,
        # pointing to different objects (game modes)

        # normalbindingtest.unregister()

    @mw.listener.binding('<ButtonPress-1>')
    def methodbindingtest(self, e):
        print(self.formatstring.format(e.x_root, e.y_root, e.widget))
GameModeManager.register_game_mode(TestGameMode)

class TestGameModeVariant(TestGameMode):
    def __init__(self):
        self.formatstring = 'variant binding\t{}\t{}\t{}'
    def game_mode_name(self):       return "Test Game Mode Variant"
GameModeManager.register_game_mode(TestGameModeVariant)
