from ..pysweep import mw

from ..gamemode import GameMode
from ..gamemodemanager import GameModeManager

from ..gamedisplay import GameDisplay, TileState
from ..playerinput import ButtonState
from ..gameinput import BoardInput, BoundaryAction

class TestBoardInput(GameMode):
    def game_mode_name(self):       return "Test BoardInput"
    def game_mode_priority(self):   return 0
    def game_mode_enable(self):
        self.gamedisplay = GameDisplay(mw.tk)
        self.boardinput = BoardInput(mw.playerinput, self.gamedisplay.displaycanvas)
        self.boardinput.register(self._handler)
    def game_mode_disable(self):
        self.gamedisplay.displaycanvas.pack_forget()
        del self.gamedisplay
        self.boardinput.unregister(self._handler)
        del self.boardinput

    def _handler(self, boardinputevent):
        # Some convenience functions
        def is_button_down(buttonstate):
            return buttonstate > ButtonState.Released
        def num_buttons_down(boardinputevent):
            i = 0
            for n in range(3):
                if is_button_down(boardinputevent.state[n]):
                    i += 1
            return i

        print(boardinputevent)
        if boardinputevent.action == BoundaryAction.Enter and num_buttons_down(boardinputevent) > 0:
            self.gamedisplay.set_tile(boardinputevent.tile, TileState.Number[0])
        elif boardinputevent.action == BoundaryAction.Leave and num_buttons_down(boardinputevent) > 0:
            self.gamedisplay.set_tile(boardinputevent.tile, TileState.Unopened)
        elif num_buttons_down(boardinputevent) > 0:
            self.gamedisplay.set_tile(boardinputevent.tile, TileState.Number[0])
        else: # num_buttons_down(boardinputevent) == 0:
            self.gamedisplay.set_tile(boardinputevent.tile, TileState.Unopened)
GameModeManager.register_game_mode(TestBoardInput)
