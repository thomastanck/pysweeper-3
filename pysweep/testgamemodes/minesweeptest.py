import math

from ..pysweep import mw

from ..gamemode import GameMode
from ..gamemodemanager import GameModeManager

from ..gamedisplay import GameDisplay, TileState, FaceState
from ..playerinput import ButtonState, ButtonAction
from ..gameinput import BoardInput, PartInput, BoardAction, BoundaryAction, BoardPressAction, ButtonPressAction
from ..boardstate import BoardState

from ..timer import Timer
from ..minesweep import Minesweep
from ..hashrand import HashRand
from ..pythonrand import PythonRand

STARTER_OPENING = True

def _in_board(tile, boardsize):
    return 0 <= tile[0] < boardsize[1] and 0 <= tile[1] < boardsize[0]
def _numtiles(boardsize):
    return boardsize[0] * boardsize[1]

class GameState:
    class BeforeStart: pass
    class Ongoing: pass
    class Lost: pass
    class Won: pass

class TestMinesweep(GameMode):
    BOARDSIZE = (30, 16)
    TARGETMINES = 99
    TIMERINTERVAL = 1
    TIMERRESOLUTION = 0.01
    RNGCLASS = PythonRand
    def game_mode_name(self):       return "Test Minesweep"
    def game_mode_priority(self):   return 0
    def game_mode_enable(self):
        self.gamedisplay = GameDisplay(mw.tk)

        self.boardinput = BoardInput(mw.playerinput, self.gamedisplay.displaycanvas)
        self.boardinput.register(self._boardinputhandler)

        self.faceinput = PartInput(mw.playerinput, self.gamedisplay.displaycanvas, self.gamedisplay.displaycanvas.display.panel.face)
        self.faceinput.register(self._faceinputhandler)

        self.boardstate = BoardState(self.BOARDSIZE)
        self.timer = Timer(mw.tk, self._timerhandler, self.TIMERINTERVAL, self.TIMERRESOLUTION)
        self.time = 0
        self.face = FaceState.Happy
        self.gamestate = GameState.BeforeStart
        self.minesweep = Minesweep(self.BOARDSIZE, self.TARGETMINES, rng=self.RNGCLASS())
        self.depressed = None
        self.chording = False

    def game_mode_disable(self):
        self.gamedisplay.displaycanvas.pack_forget()
        del self.gamedisplay
        self.boardinput.unregister(self._boardinputhandler)
        del self.boardinput
        self.faceinput.unregister(self._faceinputhandler)
        del self.faceinput
        del self.boardstate
        self.timer.kill()
        del self.timer
        del self.time
        del self.face
        del self.gamestate
        del self.minesweep
        del self.depressed
        del self.chording

    def _reset_game(self):
        self.boardstate = BoardState(self.BOARDSIZE)
        self.timer.stop()
        self.time = 0
        self.face = FaceState.Happy
        self.gamestate = GameState.BeforeStart
        self.minesweep = Minesweep(self.BOARDSIZE, self.TARGETMINES, rng=self.RNGCLASS())
        self.depressed = None
        self.chording = False

        self._updatedisplay()

    def _timerhandler(self, elapsed, sincelasttick):
        self.time = elapsed
        self._updatedisplay()

    def _faceinputhandler(self, faceinputevent):
        if faceinputevent.action == ButtonPressAction.Press:
            self.face = FaceState.Pressed
            self._updatedisplay()
        elif faceinputevent.action == ButtonPressAction.Lift:
            self.face = FaceState.Happy
            self._updatedisplay()
        elif faceinputevent.action == ButtonPressAction.Click:
            self._reset_game()

    def _boardinputhandler(self, boardinputevent):
        # Some convenience functions
        def is_button_down(buttonstate):
            return buttonstate > ButtonState.Released
        def num_buttons_down(boardinputevent):
            i = 0
            for n in range(3):
                if is_button_down(boardinputevent.state[n]):
                    i += 1
            return i

        if boardinputevent.action == BoardAction.Open:
            self._open(boardinputevent.tile)
        elif boardinputevent.action == BoardAction.Flag:
            self._toggle_flag(boardinputevent.tile)
        elif boardinputevent.action == BoardAction.Chord:
            self._chord(boardinputevent.tile)

        if boardinputevent.action == BoardPressAction.Press:
            self.depressed = boardinputevent.tile
            self.chording = False
            self._updatedisplay()
        elif boardinputevent.action == BoardPressAction.ChordPress:
            self.depressed = boardinputevent.tile
            self.chording = True
            self._updatedisplay()
        elif boardinputevent.action == BoardPressAction.Lift:
            self.depressed = None
            self.chording = False
            self._updatedisplay()

    def _open(self, tile):
        if self.gamestate == GameState.BeforeStart:
            if STARTER_OPENING:
                for i in range(-1, 2):
                    for j in range(-1, 2):
                        openingtile = (tile[0]+i, tile[1]+j)
                        if not _in_board(openingtile, self.minesweep.boardsize):
                            continue
                        self.minesweep.generate(openingtile, False)
            else:
                self.minesweep.generate(tile, False)
            self.gamestate = GameState.Ongoing
            self.timer.start()
        if self.gamestate != GameState.Ongoing:
            return
        tiles_opened = set()
        tiles_to_open = [tile,]
        while tiles_to_open:
            tile = tiles_to_open.pop()
            tiles_opened.add(tile)
            opened = self.minesweep.open(tile)
            if not opened:
                continue
            tilestate = self.minesweep.get(tile)
            self.boardstate[tile[0]][tile[1]] = tilestate
            if tilestate == TileState.Mine:
                self._lose(tile)
            elif tilestate == TileState.Flag:
                pass
            elif tilestate == TileState.Unopened:
                assert False, 'How the hell did we open a tile and get an unopened tile back?'
            elif tilestate in TileState.Number:
                tilenum = tilestate.n
                if self.minesweep.numopened + self.minesweep.targetmines == _numtiles(self.minesweep.boardsize):
                    self._win(tile)
                    self._updatedisplay()
                    return
                if tilenum == 0:
                    for i in range(-1, 2):
                        for j in range(-1, 2):
                            cascadetile = (tile[0]+i, tile[1]+j)
                            if (_in_board(cascadetile, self.minesweep.boardsize) and
                                cascadetile not in tiles_opened):
                                tiles_to_open.append(cascadetile)
            else:
                raise ValueError('Illegal tile state {}'.format(tilestate))
        self._updatedisplay()
    def _toggle_flag(self, tile):
        # print('flag', tile)
        if (self.gamestate == GameState.Lost or
            self.gamestate == GameState.Won):
            return
        tilestate = self.minesweep.toggle_flag(tile)
        self.boardstate[tile[0]][tile[1]] = tilestate
        self._updatedisplay()
    def _chord(self, tile):
        # print('chord', tile)
        if self.gamestate != GameState.Ongoing:
            return
        tilestate = self.minesweep.get(tile)
        if tilestate not in TileState.Number:
            return
        tilenum = tilestate.n

        count_gen = sum(1
            for i in range(-1, 2)
            for j in range(-1, 2)
            if _in_board((tile[0]+i, tile[1]+j), self.minesweep.boardsize) and self.minesweep.get((tile[0]+i, tile[1]+j)) == TileState.Flag)
        count = 0
        for i in range(-1, 2):
            for j in range(-1, 2):
                counttile = (tile[0]+i, tile[1]+j)
                if not _in_board(counttile, self.minesweep.boardsize):
                    continue
                if self.minesweep.get(counttile) == TileState.Flag:
                    count += 1
        assert count == count_gen
        if count == tilenum:
            for i in range(-1, 2):
                for j in range(-1, 2):
                    chordtile = (tile[0]+i, tile[1]+j)
                    if not _in_board(chordtile, self.minesweep.boardsize):
                        continue
                    self._open(chordtile)

    def _lose(self, tile):
        # print('lose', tile)
        self.gamestate = GameState.Lost
        self.timer.stop()
        self.boardstate[tile[0]][tile[1]] = TileState.Blast
        for row in range(0, self.minesweep.boardsize[1]):
            for col in range(0, self.minesweep.boardsize[0]):
                self.minesweep.autogenerate((row, col))
                if (self.minesweep.get((row, col)) == TileState.Unopened and
                    self.minesweep.is_mine((row, col))):
                    self.boardstate[row][col] = TileState.Mine
                if (self.minesweep.get((row, col)) == TileState.Flag and
                    not self.minesweep.is_mine((row, col))):
                    self.boardstate[row][col] = TileState.FlagWrong
    def _win(self, tile):
        # print('win', tile)
        self. gamestate = GameState.Won
        self.timer.stop()
        for row in range(0, self.minesweep.boardsize[1]):
            for col in range(0, self.minesweep.boardsize[0]):
                self.minesweep.autogenerate((row, col))
                if (self.minesweep.get((row, col)) == TileState.Unopened and
                    self.minesweep.is_mine((row, col))):
                    self.boardstate[row][col] = TileState.Flag

    def _updatedisplay(self):
        self.gamedisplay.set_lcounter(self.minesweep.minesleft)
        self.gamedisplay.set_rcounter(math.ceil(self.time/1000))
        self.gamedisplay.set_face(self.face)
        self.gamedisplay.set_all_tiles(self.boardstate.depress(self.depressed, self.chording))

GameModeManager.register_game_mode(TestMinesweep)
