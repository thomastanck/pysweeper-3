from ..pysweep import mw

from ..gamemode import GameMode
from ..gamemodemanager import GameModeManager

from ..gamedisplay import GameDisplay

class PlayerInputTester(GameMode):
    def game_mode_name(self):       return "PlayerInput Tester"
    def game_mode_priority(self):   return 0
    def game_mode_enable(self):
        self.gamedisplay = GameDisplay(mw.tk)
        # print(1)
        mw.playerinput.register(self.handler)
        # print(2)
        mw.playerinput.register(self.anotherhandler)
        # print(3)
    def game_mode_disable(self):
        self.gamedisplay.displaycanvas.pack_forget()
        del self.gamedisplay
        # print(1)
        mw.playerinput.unregister(self.handler)
        # print(2)
        mw.playerinput.unregister(self.anotherhandler)
        # print(3)
    def handler(self, playerinputevent):
        print(playerinputevent)
    def anotherhandler(self, playerinputevent):
        pass
GameModeManager.register_game_mode(PlayerInputTester)
