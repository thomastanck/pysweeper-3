from ..gamemodemanager import GameModeManager
from ..pysweep import mw
from ..hashrand import HashRand
from ..pythonrand import PythonRand

from .minesweeptest import TestMinesweep

class StressTest(TestMinesweep):
    TARGETMINES = 2
    RNGCLASS = HashRand
    def game_mode_name(self):       return "Minesweep Stress Test"
    def game_mode_priority(self):   return 0
    def game_mode_enable(self):
        TestMinesweep.game_mode_enable(self)
        self._updatedisplay()
    def _updatedisplay(self):
        TestMinesweep._updatedisplay(self)
        mw.tk.after(10, lambda: self._open((0, 0)))
    def _win(self, tile):
        TestMinesweep._win(self, tile)
        import sys
        sys.exit()

GameModeManager.register_game_mode(StressTest)
