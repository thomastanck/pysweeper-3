from ..pysweep import mw

from ..gamemode import GameMode
from ..gamemodemanager import GameModeManager

from ..gamedisplay import GameDisplay, TileState
from ..playerinput import ButtonState
from ..gameinput import BoardInput, BoundaryAction

from PIL import ImageDraw

class TestDraw(GameMode):
    def game_mode_name(self):       return "Test Draw"
    def game_mode_priority(self):   return 0
    def game_mode_enable(self):
        self.gamedisplay = GameDisplay(mw.tk)
        self.boardinput = BoardInput(mw.playerinput, self.gamedisplay.displaycanvas)
        self.boardinput.register(self._handler)
    def game_mode_disable(self):
        self.gamedisplay.displaycanvas.pack_forget()
        del self.gamedisplay
        self.boardinput.unregister(self._handler)
        del self.boardinput

    def _handler(self, boardinputevent):
        # Some convenience functions
        def is_button_down(buttonstate):
            return buttonstate > ButtonState.Released
        def num_buttons_down(boardinputevent):
            i = 0
            for n in range(3):
                if is_button_down(boardinputevent.state[n]):
                    i += 1
            return i

        print(boardinputevent)
        draw = ImageDraw.Draw(self.gamedisplay.displaycanvas.img)
        # draw.ellipse((92, 92, 100, 100), fill=(255,0,0))
        if boardinputevent.action == BoundaryAction.Enter:# and num_buttons_down(boardinputevent) > 0:
            # self.gamedisplay.set_tile(boardinputevent.tile, TileState.Number[0])
            anchor_x, anchor_y = self.gamedisplay.displaycanvas.display.board.tiles.position
            tilesize_x, tilesize_y = self.gamedisplay.displaycanvas.display.board.tiles.images.size
            x, y = tilesize_x * boardinputevent.tile[1] + tilesize_x/2, tilesize_y * boardinputevent.tile[0] + tilesize_y/2
            x, y = anchor_x + x, anchor_y + y
            start = x-5, y-5
            end = x+4, y+4
            draw.ellipse(start+end, fill=(255,0,0))
            self.gamedisplay.displaycanvas.update()
        elif boardinputevent.action == BoundaryAction.Leave:# and num_buttons_down(boardinputevent) > 0:
            print('settile')
            self.gamedisplay.set_tile(boardinputevent.tile, TileState.Unopened)
            self.gamedisplay.displaycanvas.draw(force=True)
            self.gamedisplay.displaycanvas.update()
        # elif num_buttons_down(boardinputevent) > 0:
        #     self.gamedisplay.set_tile(boardinputevent.tile, TileState.Number[0])
        # else: # num_buttons_down(boardinputevent) == 0:
        #     self.gamedisplay.set_tile(boardinputevent.tile, TileState.Unopened)
GameModeManager.register_game_mode(TestDraw)
