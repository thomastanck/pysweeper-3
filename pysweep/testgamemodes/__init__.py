import os, importlib
for module in os.listdir(os.path.dirname(__file__)):
    if module == '__init__.py' or module[-3:] != '.py':
        continue
    try:
        importlib.import_module('.testgamemodes.'+module[:-3], 'pysweep')
    except Exception as e:
        print(e)
del module
