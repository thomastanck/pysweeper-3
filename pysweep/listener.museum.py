def binding(trigger):
    def _decorator(func):
        """ Decorator to wrap a function or a class method to register it with Listener. """
        @functools.wraps(func)
        def _binding(*args, **kwargs):
            print(*_binding._curryargs, *args, **_binding._currykwargs, **kwargs)
            return func(*_binding._curryargs, *args, **_binding._currykwargs, **kwargs)
        _binding._trigger = trigger
        _binding._curryargs = []
        _binding._currykwargs = {}
        _binding._isactive = False
        def register(*curryargs, **currykwargs):
            _binding._isactive = True
            _binding._curryargs = curryargs
            _binding._currykwargs = currykwargs
            Listener.register(_binding._trigger, _binding)
        def unregister():
            _binding._isactive = False
            Listener.unregister(_binding._trigger, _binding)
        _binding.register = register
        _binding.unregister = unregister
        return _binding
