from .array import Array

from .gamedisplay import TileState, FaceState

def _in_board(tile, boardsize):
    return 0 <= tile[0] < boardsize[1] and 0 <= tile[1] < boardsize[0]

class BoardState:
    def __init__(self, boardsize):
        self._boardsize = boardsize
        numcols, numrows = boardsize
        self.board = Array(*boardsize, TileState.Unopened)
        # self.board = [[TileState.Unopened for i in range(numcols)] for j in range(numrows)]

    @classmethod
    def from_array(cls, board):
        numrows = len(board)
        numcols = len(board[0])
        instance = cls((numcols, numrows))
        for row in range(numrows):
            for col in range(numcols):
                instance[row][col] = board[row][col]
        return instance

    @classmethod
    def from_minesweep(cls, minesweep):
        instance = cls(minesweep.boardsize)
        numcols, numrows = minesweep.boardsize
        for row in range(numrows):
            for col in range(numcols):
                instance[row][col] = minesweep.get((row, col))
        return instance

    def __getitem__(self, key):
        return self.board[key]
    def __setitem__(self, key, value):
        self.board[key] = value
    def __len__(self):
        return len(self.board)

    def copy(self):
        """ Returns a copy of itself. """
        return BoardState.from_array(self.board)

    def paste(self, otherstate, ignore=[TileState.Unopened], inplace=False):
        """
        Pastes another board state onto this one.

        If inplace is True,
        for each tile in other state that is not in `ignore`,
        copy its value into this board state.

        If inplace is False,
        returns a new board state where the paste is done
        without modifying either self or otherstate.
        """
        modify = self if inplace else BoardState(self._boardsize)
        numcols, numrows = self._boardsize
        for row in range(numrows):
            for col in range(numcols):
                if otherstate[row][col] not in ignore:
                    modify[row][col] = otherstate[row][col]
        return modify

    def depress(self, tile, chord, inplace=False):
        """
        Adds depressed animations onto this tile.
        """
        if not tile:
            return self
        modify = self if inplace else BoardState(self._boardsize)
        numcols, numrows = self._boardsize
        if not inplace:
            for row in range(numrows):
                for col in range(numcols):
                    modify[row][col] = self[row][col]

        def _depress(tile):
            if not _in_board(tile, self._boardsize):
                return
            if modify[tile[0]][tile[1]] == TileState.Unopened:
                modify[tile[0]][tile[1]] = TileState.Number[0]
        if chord:
            for i in range(-1, 2):
                for j in range(-1, 2):
                    _depress((tile[0]+i, tile[1]+j))
        else:
            _depress(tile)
        return modify
