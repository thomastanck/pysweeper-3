import platform

from .pysweep import mw # To access mw for .after call

from .listener import unassigned_binding

class ButtonState:
    """
    poor man's enum for a button's state

    This is because actual enums either need an external pip package or
    Python 3.4+
    """

    """
    Internal use.

    Acts as a bouncer for keyboard events.
    When a key is held,
    we get key repeat events.
    Luckily we tend to receive the keydown at the same time as the keyup,
    so we can use keydown to set it to Pressed
    after the keyup sets it to Releasing.
    If the key was really released,
    a timeout sets the state from Releasing to Released.
    """
    Released =  0
    Releasing = 1
    Pressed =   2

class Button:
    """
    Indicates which mouse button the event was for.

    An event containing Any is released for each event of any of the mouse buttons.
    """
    class Any: n = -1
    class L:   n = 0
    class M:   n = 1
    class R:   n = 2

    @classmethod
    def i(cls, button_num):
        return (cls.L, cls.M, cls.R)[button_num]

class Action:
    """
    Indicates if the event represents
    that a mouse button was pressed: 'D'
    or the mouse was moved as the mouse button was held: 'M'
    or the mouse button was released: 'U'
    """
    class D: pass
    class M: pass
    class U: pass

class ButtonAction:
    """
    Button + Action together.
    'D', 'M', 'U' events are emitted regardless of the mouse button(s) pressed.
    """
    class D: pass # If any mouse button was pressed
    class M: pass # Emitted even if there aren't any mouse buttons pressed
    class U: pass # If any mouse button was released
    class LD: pass
    class LM: pass
    class LU: pass
    class MD: pass
    class MM: pass
    class MU: pass
    class RD: pass
    class RM: pass
    class RU: pass

    @classmethod
    def from_button_action(cls, button, action):
        if action == Action.D:
            return (cls.D, cls.LD, cls.MD, cls.RD)[button.n+1]
        elif action == Action.M:
            return (cls.M, cls.LM, cls.MM, cls.RM)[button.n+1]
        elif action == Action.U:
            return (cls.U, cls.LU, cls.MU, cls.RU)[button.n+1]
        else:
            raise ValueError('Invalid button/action combination ({}, {})'.format(button, action))

class InputType:
    """
    Internal use.

    Indicates if the event originates from a keyboard or mouse event.
    """
    class Mouse:    n = 0
    class Keyboard: n = 1

class PlayerInputEvent:
    def __init__(self, button, action, root_position, state):
        self.button = button
        self.action = action
        self.buttonaction = ButtonAction.from_button_action(self.button, self.action)
        self.root_position = root_position
        self.state = state

    def __str__(self):
        return '{'+"button:'{}',action:'{}',root_position:{},state:{}".format(self.button, self.action, self.root_position, self.state)+'}'

    def __repr__(self):
        return str(self)

class PlayerInput:
    """
    A class that keeps track of input events.

    Does NOT create board events like tile down,
    those are handled by ClickManager.
    """
    def __init__(self, listener, settings=None):
        self._listener = listener

        self._motion.init(listener)
        self._keyd.init(listener)
        self._keyu.init(listener)
        self._b1d.init(listener)
        self._b1u.init(listener)
        self._b2d.init(listener)
        self._b2u.init(listener)
        self._b3d.init(listener)
        self._b3u.init(listener)

        # settings = [LMB key/mouse button, MMB key/mouse button, RMB key/mouse button]
        if settings:
            self.settings = settings
        else:
            # Default settings
            if platform.system() == 'Darwin':  # How Mac OS X is identified by Python
                self.settings = [
                    [1, 3, 2], # OSX uses mouse button 2 for right click. weird huh?
                    ['3', None, '2'],
                ]
                self.mouse_settings    = self.settings[InputType.Mouse.n]
                self.keyboard_settings = self.settings[InputType.Keyboard.n]
            else:
                self.settings = [
                    [1, 2, 3], # Everyone else uses mouse button 3 for right click.
                    ['3', None, '2'],
                ]
                self.mouse_settings    = self.settings[InputType.Mouse.n]
                self.keyboard_settings = self.settings[InputType.Keyboard.n]

        self.raw_state = [[ButtonState.Released] * 2] + [[ButtonState.Released] * 2] + [[ButtonState.Released] * 2]
        self.state = [ButtonState.Released] * 3
        self.root_position = (0, 0) # Relative to screen

        self._callbacks = []

    def register(self, callback):
        if len(self._callbacks) == 0:
            self._register_bindings()
        self._callbacks.append(callback)

    def unregister(self, callback):
        self._callbacks.remove(callback)
        if len(self._callbacks) == 0:
            self._unregister_bindings()

    def _register_bindings(self):
        self._motion.register()
        self._keyd.register()
        self._keyu.register()
        self._b1d.register()
        self._b1u.register()
        self._b2d.register()
        self._b2u.register()
        self._b3d.register()
        self._b3u.register()

    def _unregister_bindings(self):
        self._motion.unregister()
        self._keyd.unregister()
        self._keyu.unregister()
        self._b1d.unregister()
        self._b1u.unregister()
        self._b2d.unregister()
        self._b2u.unregister()
        self._b3d.unregister()
        self._b3u.unregister()

    @unassigned_binding('<Motion>')
    def _motion(self, event):
        self.root_position = (event.x_root, event.y_root)
        self.move()

    @unassigned_binding('<KeyPress>')
    def _keyd(self, event):
        if event.char in self.keyboard_settings:
            self.down(Button.i(self.keyboard_settings.index(event.char)), InputType.Keyboard)
    @unassigned_binding('<KeyRelease>')
    def _keyu(self, event):
        if event.char in self.keyboard_settings:
            self.tryup(Button.i(self.keyboard_settings.index(event.char)), InputType.Keyboard)
    @unassigned_binding('<ButtonPress-1>')
    def _b1d(self, event): self.down(Button.i(self.mouse_settings.index(1)), InputType.Mouse)
    @unassigned_binding('<ButtonRelease-1>')
    def _b1u(self, event): self.tryup(Button.i(self.mouse_settings.index(1)), InputType.Mouse)
    @unassigned_binding('<ButtonPress-2>')
    def _b2d(self, event): self.down(Button.i(self.mouse_settings.index(2)), InputType.Mouse)
    @unassigned_binding('<ButtonRelease-2>')
    def _b2u(self, event): self.tryup(Button.i(self.mouse_settings.index(2)), InputType.Mouse)
    @unassigned_binding('<ButtonPress-3>')
    def _b3d(self, event): self.down(Button.i(self.mouse_settings.index(3)), InputType.Mouse)
    @unassigned_binding('<ButtonRelease-3>')
    def _b3u(self, event): self.tryup(Button.i(self.mouse_settings.index(3)), InputType.Mouse)

    def down(self, button, inputtype):
        self.raw_state[button.n][inputtype.n] = ButtonState.Pressed
        if self.state[button.n] == ButtonState.Released:
            self.state[button.n] = max(self.raw_state[button.n])
            self.D()
            if button == Button.L: self.LD()
            if button == Button.M: self.MD()
            if button == Button.R: self.RD()
    def move(self):
        self.M()
        if self.state[0] > ButtonState.Released: self.LM()
        if self.state[1] > ButtonState.Released: self.MM()
        if self.state[2] > ButtonState.Released: self.RM()
    def tryup(self, button, inputtype):
        if inputtype == InputType.Mouse:
            self.raw_state[button.n][inputtype.n] = ButtonState.Released
        elif inputtype == InputType.Keyboard:
            self.raw_state[button.n][inputtype.n] = ButtonState.Releasing
            mw.tk.after(0, lambda button=button: self.actuallykeyup(button))
        else:
            raise ValueError("{} is not an InputType.".format(inputtype))
        self.up(button)
    def actuallykeyup(self, button):
        if self.raw_state[button.n][InputType.Keyboard.n] == ButtonState.Releasing:
            self.raw_state[button.n][InputType.Keyboard.n] = ButtonState.Released
        self.up(button)
    def up(self, button):
        self.state[button.n] = max(self.raw_state[button.n])
        if self.state[button.n] == ButtonState.Released:
            self.U()
            if button == Button.L: self.LU()
            if button == Button.M: self.MU()
            if button == Button.R: self.RU()

    def D(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.Any, Action.D, self.root_position, self.state)  )
    def M(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.Any, Action.M, self.root_position, self.state)  )
    def U(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.Any, Action.U, self.root_position, self.state)  )

    def LD(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.L, Action.D, self.root_position, self.state)    )
    def LM(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.L, Action.M, self.root_position, self.state)    )
    def LU(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.L, Action.U, self.root_position, self.state)    )

    def MD(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.M, Action.D, self.root_position, self.state)    )
    def MM(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.M, Action.M, self.root_position, self.state)    )
    def MU(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.M, Action.U, self.root_position, self.state)    )

    def RD(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.R, Action.D, self.root_position, self.state)    )
    def RM(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.R, Action.M, self.root_position, self.state)    )
    def RU(self):
        for callback in self._callbacks:
            callback(PlayerInputEvent(Button.R, Action.U, self.root_position, self.state)    )
