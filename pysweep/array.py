import itertools # for itertools.product

class Array1d:
    """
    A 2d-arary like object with a bunch of convenience functions.
    """
    def __init__(self, numcols, numrows, default=None):
        self._numcols = numcols
        self._numrows = numrows
        self._size = (numcols, numrows)
        self._numtiles = numrows * numcols
        self._array = [default] * self._numtiles
    @classmethod
    def from_list(cls, array):
        numrows = len(array)
        numcols = len(array[0])
        instance = cls(numcols, numrows)
        for i, row in enumerate(array):
            instance[i] = row
        return instance
    def __getitem__(self, key):
        if isinstance(key, slice):
            raise NotImplementedError # How the fuck do we handle fucking slices?!
        elif isinstance(key, tuple):
            return self.get(*key)
        else:
            class ProxyRow:
                def __getitem__(innerself, key2):
                    return self._array[key*self._numcols + key2]
                def __setitem__(innerself, key2, value):
                    self._array[key*self._numcols + key2] = value
                def __len__(innerself):
                    return self._numcols
            return ProxyRow()
            # return self._array[key*self._numcols:(key+1)*self._numcols]
    def __setitem__(self, key, value):
        if isinstance(key, slice):
            raise NotImplementedError # How the fuck do we handle fucking slices?!
        elif isinstance(key, tuple):
            self.set(*key, value)
        else:
            self._array[key*self._numcols:(key+1)*self._numcols] = value

    def __len__(self):
        return self._numrows
    def __contains__(self, key):
        return self.inbounds(*key)

    @property
    def size(self):
        return self._size
    @property
    def numcols(self):
        return self._numcols
    @property
    def numrows(self):
        return self._numrows
    @property
    def numtiles(self):
        return self._numtiles

    def indextorowcol(self, index):
        return index//self._numcols, index%self._numcols

    def get(self, row, col):
        return self._array[row*self._numcols + col]
    def set(self, row, col, value):
        self._array[row*self._numcols + col] = value

    def neighbours(self, row, col):
        yield from (
                (row-1, col-1), (row-1, col+0), (row-1, col+1),
                (row+0, col-1),                 (row+0, col+1),
                (row+1, col-1), (row+1, col+0), (row+1, col+1),
            )
    def neighbour_values(self, row, col):
        yield from (
                self.get(row-1, col-1), self.get(row-1, col+0), self.get(row-1, col+1),
                self.get(row+0, col-1),                         self.get(row+0, col+1),
                self.get(row+1, col-1), self.get(row+1, col+0), self.get(row+1, col+1),
            )
    def neighbour_items(self, row, col):
        for neighbour in (
                (row-1, col-1), (row-1, col+0), (row-1, col+1),
                (row+0, col-1),                 (row+0, col+1),
                (row+1, col-1), (row+1, col+0), (row+1, col+1),
            ):
            yield (neighbour, self.get(*neighbour))

    def inbounds(self, row, col):
        return 0 <= row < self._numrows and 0 <= col < self._numcols

    def __iter__(self):
        yield from itertools.product(range(self._numrows), range(self._numcols))
    def keys(self):
        yield from itertools.product(range(self._numrows), range(self._numcols))
    def values(self):
        yield from self._array
    def items(self):
        yield from ((self.indextorowcol(i), v) for i, v in enumerate(self._array))

class Array2d:
    """
    A 2d-arary like object with a bunch of convenience functions.
    """
    def __init__(self, numcols, numrows, default=None):
        self._numcols = numcols
        self._numrows = numrows
        self._size = (numcols, numrows)
        self._numtiles = numrows * numcols
        self._array = [[default] * numcols for i in range(numrows)]
    @classmethod
    def from_list(cls, array):
        numrows = len(array)
        numcols = len(array[0])
        instance = cls(numcols, numrows)
        for i, row in enumerate(array):
            instance[i] = row
        return instance
    def __getitem__(self, key):
        if isinstance(key, tuple):
            return self.get(*key)
        else:
            return self._array[key]
    def __setitem__(self, key, value):
        if isinstance(key, tuple):
            self.set(*key, value)
        else:
            self._array[key] = value

    def __len__(self):
        return self._numrows
    def __contains__(self, key):
        return self.inbounds(*key)

    @property
    def size(self):
        return self._size
    @property
    def numcols(self):
        return self._numcols
    @property
    def numrows(self):
        return self._numrows
    @property
    def numtiles(self):
        return self._numtiles

    def get(self, row, col):
        return self._array[row][col]
    def set(self, row, col, value):
        self._array[row][col] = value

    def neighbours(self, row, col):
       for r,c in (
                (row-1, col-1), (row-1, col+0), (row-1, col+1),
                (row+0, col-1),                 (row+0, col+1),
                (row+1, col-1), (row+1, col+0), (row+1, col+1),
            ):
            if self.inbounds(r,c):
                yield (r,c)
    def neighbour_values(self, row, col):
       for r,c in (
                (row-1, col-1), (row-1, col+0), (row-1, col+1),
                (row+0, col-1),                 (row+0, col+1),
                (row+1, col-1), (row+1, col+0), (row+1, col+1),
            ):
            if self.inbounds(r,c):
                yield self.get(r,c)
    def neighbour_items(self, row, col):
       for r,c in (
                (row-1, col-1), (row-1, col+0), (row-1, col+1),
                (row+0, col-1),                 (row+0, col+1),
                (row+1, col-1), (row+1, col+0), (row+1, col+1),
            ):
            if self.inbounds(r,c):
                yield ((r,c), self.get(r,c))

    def inbounds(self, row, col):
        return 0 <= row < self._numrows and 0 <= col < self._numcols

    def __iter__(self):
        yield from itertools.product(range(self._numrows), range(self._numcols))
    def keys(self):
        yield from itertools.product(range(self._numrows), range(self._numcols))
    def values(self):
        for row in self._array:
            yield from row
    def items(self):
        for r, row in enumerate(self._array):
            for c, val in enumerate(row):
                yield ((r, c), val)

def Array2dPlain(numcols, numrows, default=None):
    return [[default] * numcols for i in range(numrows)]

Array=Array2d
