class GameMode:
    """
    Base class for all game modes.

    Instantiated on startup, defaulting to disabled.
    enable/disable called when the game mode is enabled/disabled via the menu.
    On enable, game mode is allowed to touch the main window.
    It should also connect to the listener and set up menus.
    On disable, game mode should clean up the main window.
    It should also unregister from the listener and remove menus.
    """
    def __init__(self):
        """ Called after the main window has been set up """
        pass
    def game_mode_name(self):
        """ Returns the label to be placed in the game mode selector menu """
        raise NotImplementedError()
    def game_mode_priority(self):
        """ Returns the priority (order) the game mode should have when placing in the menu """
        raise NotImplementedError()
    def game_mode_enable(self):
        """ Called when the game mode is enabled """
        raise NotImplementedError()
    def game_mode_disable(self):
        """ Called when the game mode is disabled """
        raise NotImplementedError()
