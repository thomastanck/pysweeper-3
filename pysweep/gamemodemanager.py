import tkinter

import bisect

from .pysweep import mw

class GameModeManager:
    _initd = False
    _gamemodeclasses = []
    _initdgamemodeclasses = set()
    _gamemodes = {}
    _gamemodelist = []
    _currentgamemode = None
    _gamemodemenu = None
    _gamemodemenuvar = None

    def register_game_mode(gamemodeclass):
        """ Call this at import time per game mode """
        GameModeManager._gamemodeclasses.append(gamemodeclass)
        if GameModeManager._initd:
            GameModeManager._init_game_mode(gamemodeclass)


    def init_game_mode_manager():
        """ Call this after menu's been init'd! """
        GameModeManager._initd = True

        menu = GameModeManager._gamemodemenu = tkinter.Menu(mw.menu.menubar, tearoff=0)
        menuvar = GameModeManager._gamemodemenuvar = tkinter.StringVar()

        for cls in GameModeManager._gamemodeclasses:
            GameModeManager._init_game_mode(cls)

        mw.menu.add_menu("Game", menu)

        menu.invoke("Test Minesweep")
        # menu.invoke("Minesweep Stress Test")

    def _init_game_mode(gamemodeclass):
        if gamemodeclass in GameModeManager._initdgamemodeclasses:
            return
        instance = gamemodeclass()
        name = instance.game_mode_name()
        priority = instance.game_mode_priority()
        GameModeManager._gamemodes[name] = instance
        i = bisect.bisect(GameModeManager._gamemodelist, (priority, name, instance))
        GameModeManager._gamemodelist.insert(i, (priority, name, instance))
        def command(name=name):
            GameModeManager._set_game_mode(name)
        GameModeManager._gamemodemenu.insert_radiobutton(i, label=name, variable=GameModeManager._gamemodemenuvar, command=command)

    def _set_game_mode(name):
        if GameModeManager._currentgamemode:
            GameModeManager._gamemodes[GameModeManager._currentgamemode].game_mode_disable()
        GameModeManager._currentgamemode = name
        GameModeManager._gamemodes[GameModeManager._currentgamemode].game_mode_enable()
